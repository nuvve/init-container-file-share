FROM debian:10.1-slim

# Copyright 2019 Nuvve Corporation
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 

RUN mkdir -p /usr/share/man/man1
RUN apt-get update
RUN apt-get install -y unzip curl python

# Install AWS CLI Tool
RUN curl -OL "https://s3.amazonaws.com/aws-cli/awscli-bundle.zip" -o "awscli-bundle.zip"
RUN unzip awscli-bundle.zip
RUN ./awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws

COPY target/startup.sh /startup.sh
RUN chmod 755 /startup.sh

ENTRYPOINT ["/startup.sh"]
