# init-container-file-share

This container equipped with AWS S3 access rights that allows it download secure file(s) and install it in a directory location that is share-able with another container (target container) running in the same ECS fargate task. The sole purpose of this task is to download the contents of an S3 folder (containing files/subdirectories) into a directory that is shared with the target container. The target container will not have any access to S3.  Since this container has less attack points, it is considered safer than having the other container directly access S3.  This container serves as a template for many ***side-car*** uses.

# How this works:

This docker container runs a script that copies contents of a S3 folder into a "shared" directory.
The S3 bucket folder and the target folder are specified as parameters when running the container.
The target folder is created by the script if it does not exist.
The taskRoleArn for the task should provide the container access to S3 to copy the directory location specified as a cmd line parameter.
This container is run alongside a "target" container in an ECS task and the "shared" directory is shared across the two containers.
This allows the "target" container to access files in the "shared" directory (which were originally from a S3 bucket) without requiring any AWS S3 credentials.

The idea is similar to Kubernetes init-containers: https://kubernetes.io/docs/concepts/workloads/pods/init-containers/

# Usage:

- Create S3 bucket to store a directory containing all files/dir structure that needs to be shared with the target container
- Obtains AWS Key ID/Access Key which has Read access to the S3 bucket mentioned above via Task Definition taskRoleArn
- Include this container in the same task definition file as the target container.
- Run this container by
	- Define the docker CMD as providing the parameters that is passed into the default ENTRYPOINT command. Parameters are < S3 bucket folder> < Local destination folder>

- The ECS task definition should define taskRoleArn that has access to the S3 bucket where the files will be copied from.
- Modify the Task defintion as follows:
	- To avoid race conditions: Use task definition dependsOn to start this container first prior to the "target" container that uses the shared file(s). See https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task_definition_parameters.html
	- Define taskRoleArn that allows the container to copy contents of S3 bucket specified in the cmd line parameter to a local folder.