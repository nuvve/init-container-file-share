#!/bin/bash
#
# Copyright 2019 Nuvve Corporation
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Usage: startup.sh <S3 directory> <local destination>
#.       S3 directory - where to copy the files from.
#.       local destination - Local directory where the S3 file(s) are copied to
# e.g startup.sh s3://container_data /var/shared

RUN mkdir -p /usr/share/man/man1
RUN apt-get update
RUN apt-get install -y unzip curl python

# Install AWS CLI Tool
RUN curl -OL "https://s3.amazonaws.com/aws-cli/awscli-bundle.zip" -o "awscli-bundle.zip"
RUN unzip awscli-bundle.zip
RUN ./awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws

COPY target/startup.sh /startup.sh
RUN chmod 755 /startup.sh

ENTRYPOINT ["/startup.sh"]


# 
# Assumes that this container is run with an ECS task definition file with a taskRoleArn that allows access to the S3 bucket
# directory specified as the 1st parameter to this script.  It then copies the files from that S3 folder location to a local
# folder specified in the 2nd parameter.

if [ -n "$1" ]; 
then
    if [ -n "$2" ];
    then
        mkdir -p $2
        # assumes AWS credentials are stored in user's home directory under .aws
        aws s3 cp --recursive $1 $2 --recursive || echo "Copying AWS S3 file(s) to $2 failed. Return code=$?"
    else
        echo "No destination folder specified. Using /var/shared"
        mkdir -p /var/shared
        # assumes AWS credentials are stored in user's home directory under .aws
        aws s3 cp $1 /var/shared --recursive || echo "Copying AWS S3 file(s) to /var/shared failed. Return code=$?"
    fi
else
    echo "No parameters found!"
fi
